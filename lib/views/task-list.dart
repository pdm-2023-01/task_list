import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TaskListPage extends StatelessWidget {

  FirebaseFirestore firestore = FirebaseFirestore.instance;
  FirebaseAuth auth = FirebaseAuth.instance;

  void update(String id, bool finished) {
    // UPDATE tasks SET finished = ? WHERE id = ?
    firestore.collection('tasks').doc(id).update({'finished': finished});
  }

  void delete(String id) {
    firestore.collection('tasks').doc(id).delete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tasks"),
      ),
      body: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
        stream: firestore
          .collection('tasks')
          .where('uid', isEqualTo: auth.currentUser!.uid)
          .snapshots(),
        builder: (context, snapshot) {

          if(!snapshot.hasData) {
            return CircularProgressIndicator();
          }

          var tasks = snapshot.data!.docs;

          return ListView(
            children: tasks.map((task) => 

              Dismissible(
                key: Key(task.id),
                onDismissed: (_) => delete(task.id),
                background: Container(color: Colors.red),
                child: CheckboxListTile(
                  title: Text(task['name']),
                  onChanged: (value) => update(task.id, value!),
                  value: task['finished']
                ),
              )

            ).toList(),
          );
        }
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context)
          .pushNamed('/task-create'),
        child: Icon(Icons.add),
      ),
    );
  }
}