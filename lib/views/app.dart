import 'package:flutter/material.dart';

import 'task-create.dart';
import 'task-list.dart';
import 'user-login.dart';
import 'user-register.dart';

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/task-list':(context) => TaskListPage(),
        '/task-create': (context) => TaskCreatePage(),
        '/user-login':(context) => UserLoginPage(),
        '/user-register':(context) => UserRegisterPage(),
      },
      initialRoute: '/user-login',
    );
  }
}