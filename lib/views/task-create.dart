import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class TaskCreatePage extends StatelessWidget {

  // <input id='abc' name='def'.. == GlobalKey
  var formKey = GlobalKey<FormState>();

  String name = '';
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  FirebaseAuth auth = FirebaseAuth.instance;

  void salvar(BuildContext context) {

    if(formKey.currentState!.validate()) {

      formKey.currentState!.save();

      // salvar os dados no banco de dados...
      firestore
        .collection('tasks')
        .add({
          'name': name, 
          'finished': false,
          'uid': auth.currentUser!.uid,
        });

      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("New Task"),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(              
                autovalidateMode: AutovalidateMode.always,
                minLines: 1,
                maxLines: 5,
                maxLength: 200,
                decoration: InputDecoration(
                  hintText: "O que precisa fazer?",
                ),
                onSaved: (value) => name = value!,
                validator: (value) {
                  if(value!.isEmpty) {
                    return "Campo obrigatório.";
                  }
                  return null;
                },
              ),
              Container(
                width: MediaQuery.of(context).size.width - 40,
                child: ElevatedButton(
                  onPressed: () => salvar(context), 
                  child: const Text("Salvar"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}