import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class UserLoginPage extends StatelessWidget {

  FirebaseAuth auth = FirebaseAuth.instance;

  var formKey = GlobalKey<FormState>();

  String email = '';
  String password = '';

  void login(BuildContext context) async {

    if(formKey.currentState!.validate()) {
      formKey.currentState!.save();
      try {
        // fazer o register usando firebase auth
        await auth.signInWithEmailAndPassword(email: email, password: password);

        Navigator.of(context).pushNamed('/task-list');
      }
      catch (e) {
        print(e);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(              
                minLines: 1,
                decoration: InputDecoration(
                  labelText: "E-mail",
                ),
                onSaved: (value) => email = value!,
                validator: (value) {
                  if(value!.isEmpty) {
                    return "Campo obrigatório.";
                  }
                  return null;
                },
              ),
              TextFormField(              
                minLines: 1,
                decoration: InputDecoration(
                  labelText: "Senha",
                ),
                onSaved: (value) => password = value!,
                validator: (value) {
                  if(value!.isEmpty) {
                    return "Campo obrigatório.";
                  }
                  if(value.length < 6) {
                    return "Campo deve conter no mínimo 6 caracteres.";
                  }
                  return null;
                },
              ),
              Container(
                width: MediaQuery.of(context).size.width - 40,
                child: ElevatedButton(
                  onPressed: () => login(context), 
                  child: const Text("Entrar"),
                ),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pushNamed('/user-register'),
                child: const Text("Registrar"),
              )
            ],
          ),
        ),
      ),
    );
  }
}