import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'views/app.dart';

const firebaseConfig = FirebaseOptions(
  apiKey: "AIzaSyBtYC2dEfbd2UaCoZMQXcgPkcYJKv5OmNA",
  authDomain: "tasklist-553d9.firebaseapp.com",
  projectId: "tasklist-553d9",
  storageBucket: "tasklist-553d9.appspot.com",
  messagingSenderId: "1016832821917",
  appId: "1:1016832821917:web:4fb6cfb9909a15f1e16274"
);

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: firebaseConfig);
  runApp(App());
}
